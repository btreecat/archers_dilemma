/*
 * main.c
 *
 *  Created on: Apr 2, 2012
 *      Author: stanner
 *      Copyright   : This file is part of archers_dilemma.

                archers_dilemma is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                archers_dilemma is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with archers_dilemma.  If not, see <http://www.gnu.org/licenses/>.
 Description : Attempts to find Lana inside ODIN HQ
 */

#include "queue.h"

//Floor struct
struct Floor {
	int num;
	struct Floor *up;
	struct Floor *down;
};

//Function Prototypes
int goUp (int floor, int up, int ceiling);
int goDown (int floor, int down);
void queUp(struct Queue *q, struct Floor *f, struct Queue *set);
void queDown(struct Queue *q, struct Floor *f, struct Queue *set);
struct Floor *newFloor(int i);

int main (void) {

	//


	int S,N,G,U,D = 0;
	//N = 10;
	//S = 1;
	//G = 7;
	//U = 4;
	//D = 2;
	printf("Please input the values in this order (with spaces): N S G U D\n");
	scanf("%d %d %d %d %d", &N, &S, &G, &U, &D);

	struct Queue newQ;
	init(&newQ);

	struct Queue set;
	init(&set);


	//Queue the root node
	enqueue(&newQ, S);


	struct Floor nn, *node = &nn;
	while (!isEmpty(&newQ)) {
		//dequeue current node
		int f = dequeue(&newQ);
		enqueue(&set, f);

		if (f == G) {
			break;
		}


		//generate new states for up and down nodes

		node = newFloor(f);
		//node.num = f;
		int up = goUp(node->num, U, N);
		int down = goDown(node->num, D);
		node->up = newFloor(up);
		node->down = newFloor(down);

		//add new nodes to queue
		if (!contains(&set, up)) {
			queUp(&newQ, node, &set);

		}
		if(!contains(&set, down)) {
			queDown(&newQ, node, &set);
		}


	}

	if (contains(&set, G)) {
		printf("Danger zone.\n");
	}
	else {
		printf("LANA");
	}

	return 0;


}

void queUp(struct Queue *q, struct Floor *f, struct Queue *set) {
	//Add the floor found by goin up, to the queue
	if (f->up->num != -1) {
		int num = f->up->num;
		enqueue(q, num);
		enqueue(set, num);
	}
}
void queDown(struct Queue *q, struct Floor *f, struct Queue *set) {
	//Add the floor found by goin down, to the queue
	if (f->down->num != -1) {
		int num = f->down->num;
		enqueue(q, num);
		enqueue(set, num);
	}
}

int goUp (int floor, int up, int ceiling) {
	//Calculate the floor found by pressing the up button
	if (floor + up >= ceiling) {
		return -1;
	}
	else {
		return floor + up;
	}
}

int goDown (int floor, int down) {
	//Calculate the floor found by pressing the down button
	if (floor - down < 0) {
		return -1;
	}
	else {
		return floor - down;
	}
}

struct Floor *newFloor(int i) {
	//Create a new floor on the fly for added to the queue and set
	struct Floor floor, *ff = &floor;
	ff = (struct Floor *) malloc(sizeof(struct Floor));
	ff->num = i;
	ff->down = (void *) -1;
	ff->up = (void *) -1;
	return ff;
}
