/*
 * main.c
 *
 *  Created on: Apr 2, 2012
 *      Author: stanner
 *      Copyright   : This file is part of archers_dilemma.

                archers_dilemma is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                archers_dilemma is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with archers_dilemma.  If not, see <http://www.gnu.org/licenses/>.
 Description : Attempts to find Lana inside ODIN HQ
 */

#include "queue.h"
//#include <stdlib.h>


void init(struct Queue *q) {
	/* This method will clear the pointers to the front and rear of the queue */
	q->front = NULL;
	q->rear = NULL;
}

void enqueue(struct Queue *q, int data) {
	/* This method will take a queue and an int, and add the int to the queue */
	struct Node *newNode;
	newNode = (struct Node *) malloc(sizeof(struct Node));
	newNode->data = data;
	newNode->next = NULL;

	if (isEmpty(q)) {
		q->front = newNode;

	}
	else {
		q->rear->next = newNode;
	}

	q->rear = newNode;

	size(q);

}

int dequeue(struct Queue *q) {
	/* This method is to pop off the head of the queue and update the head*/
	if (isEmpty(q)) {
		return 0;
	}

	struct Node *nn;

	nn = q->front;

	int data = nn->data;

	if (size(q) == 1) {
		q->rear = NULL;
		init(q);
	}

	q->front = nn->next;



	free(nn);

	nn = NULL;

	return data;
}


int peekFront(struct Queue *q) {
	/* Look at the front of the queue without removing it */
	return q->front->data;
}
int peekRear(struct Queue *q) {
	/* Look at the back of the queue without removing it */
	return q->rear->data;
}


int contains(struct Queue *q, int data) {
	/* Check the queue to see if it contains the element data */
	struct Node *nn;
	nn = q->front;

	while (nn != NULL) {
		if (nn->data == data) {
			return 1;
		}
		nn = nn->next;
	}

	return 0;
}
int isEmpty(struct Queue *q) {
	/* Rreturns true if the Queue is empty */
	if (q->front == NULL) {
		return 1;
	}
	else {
		return 0;
	}
}
int size(struct Queue *q) {
	/* Counts the number of elements in a queue */
	struct Node *nn;
	nn = q->front;
	int count = 0;
	while (nn != NULL) {
		count++;
		nn = nn->next;
	}
	return count;
}










