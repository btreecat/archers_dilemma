

#include <stdio.h>
#include <stdlib.h>
/*
 * Struct representing the Queue.
 * Contains a pointer to the front Node and the rear Node.
 */
struct Queue {
	struct Node *front;
	struct Node *rear;
};


/*
 * Struct representing a Queue Node.
 * Contains an int data element and a pointer to the next
 * Node struct.
 */
struct Node {
	int data;
	struct Node *next;
};


/* Initializes the Queue's front and rear pointers to NULL */
void init(struct Queue *q);

/* Add a new int to the rear of the Queue */
void enqueue(struct Queue *q, int data);

/* Remove int from the front of the Queue */
int dequeue(struct Queue *q);

/* Returns the front integer without removing it */
int peekFront(struct Queue *q);

/* Returns the back integer without removing it */
int peekRear(struct Queue *q);

/* Returns 1 if the current int is in the Queue, otherwise 0 */
int contains(struct Queue *q, int data);

/* Returns 1 if the Queue is empty, otherwise 0 */
int isEmpty(struct Queue *q);

/* Returns the size of the Queue */
int size(struct Queue *q);








